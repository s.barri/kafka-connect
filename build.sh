DEBEZIUM_LINK=https://repo1.maven.org/maven2/io/debezium/debezium-connector-postgres/1.3.1.Final/debezium-connector-postgres-1.3.1.Final-plugin.tar.gz
ELASTICSEARCH_LINK=https://d1i4a15mxbxib1.cloudfront.net/api/plugins/confluentinc/kafka-connect-elasticsearch/versions/10.0.2/confluentinc-kafka-connect-elasticsearch-10.0.2.zip

mkdir plugins
wget -c $DEBEZIUM_LINK -O debezium.tar.gz
wget -c $ELASTICSEARCH_LINK -O elasticsearch.zip
tar -xzvf debezium.tar.gz -C plugins/
unzip elasticsearch.zip -d plugins/

#Docker create for gitlab-ci
docker pull $CI_REGISTRY_IMAGE:latest || true
docker build --cache-from $CI_REGISTRY_IMAGE:latest --tag $CI_REGISTRY_IMAGE:latest .
docker push $CI_REGISTRY_IMAGE:latest