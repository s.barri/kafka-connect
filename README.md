Kafka Connect Image
====================

In order to use the Kafka Connect with any plugin with Strimzi Kafka we need to create a custom image with the latest strimzi/kafka image.
The GitLabCi will build and push the image to its Repository.
<h2>Debezium</h2>
For the debezium plugin we need to visit https://debezium.io/documentation/reference/install.html and find the latest stable release of the Postgres Connector plugin and use the download link in our dockerfile.

<h2>ElasticSearch</h2>
For the elasticsearch plugin we need to visit https://docs.confluent.io/current/connect/kafka-connect-elasticsearch/index.html and find the download link and use it in our dockerfile.